# Simple Static

Simple_Static powered by Python is a tool that you can used to create static websites for very low computers.

The idea came from Pelican and LowTechMagazine.

## For who is this tool

This tool is made for people who wants to get full control over what's done behind their static website. Also for those who wants to know how it works in the background without needing to read thousands of lines of code.

Also for people who wants to get a simple static website and no other thing.

This is a project you can also use to build your own version of this static website. All is in one file so you can configure them easily. 

For the moment there is no more configuration. The configuration of the template 'base' is done manually by adding the stuff inside the template 'base.html' excepted for the menu (beta feature) that will be generated automatically.

## Features

* Easy templating using Jinja
* Written in Python and easy to understand
* Low-Tech websites
* Lightweight app and websites
* Dither the images automatically and convert them to PNG
* All-in-one file to make the process easier. Just run it and upload your files!
* 100% free software.

## To use this tool 

### Using the default theme

1. Clone this repository
2. Put the articles in HTML format (you can use MarkText for that to export it into HTML (without styles) and name it with the number of the article) into the folder 'content'
3. Run the file app.py to generate the ouput into the folder 'output'
4. You can test it by opening 'index.html' in your browser. You can change the name and the menu in the template 'base.html'
5. Send it to your server. This can run on very low servers with a very low electricity supply.

### Make your own theme

1. Clone this repo
2. Check the file 'base.html' first to change the name and the menu. You can also add what ever you want.
3. When you go on your website, you can go into the 'inspector mode' > sources > style.css to edit the style of your website in live **do not reload or all your changes will be lost**
4. When you are happy with the result, you can copy and paste the content into the file 'content/style.css' and regenerate the website again.

### Add images

To add images you just need to add them into the 'content' directory, everything will be done automatically to dither them. 

### Change the configuration (fork)

This section cannot be done for know because the app is still in beta.

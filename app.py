import os
from jinja2 import FileSystemLoader, Environment
from shutil import copyfile
from PIL import Image
import hitherdither

# Don't care about these, these are only modules
def inject(data, input, to):
    env = Environment(loader=FileSystemLoader(os.path.join("theme")))
    template = env.get_template(input)
    open(to, "w").write(template.render(data=data))


def dither(origin, output):
    size = (800, 800)
    palette = hitherdither.palette.Palette(
        [
            (25, 25, 25),
            (75, 75, 75),
            (125, 125, 125),
            (175, 175, 175),
            (225, 225, 225),
            (250, 250, 250),
        ]
    )
    threshold = [96, 96, 96]

    img = Image.open(origin)
    img.thumbnail(size, Image.LANCZOS)
    img = hitherdither.ordered.bayer.bayer_dithering(img, palette, threshold, order=8)
    if output.endswith(".jpg"):
        output = output.split(".jpg")[0] + ".png"
    img.save(output)


def gett(stuff, pre, suff):
    return stuff.split(pre)[1].split(suff)[0]


# Define what an article is
class Article(object):
    def __init__(self, title, body, category, author, date):
        self.title = title
        self.body = body
        self.category = category
        self.author = author
        self.date = date


# Define the different key lists, how could you organize the articles?
articles, categories, authors = [], {}, {}

# List all filetypes in a directory
for f in os.listdir("content"):
    if f.endswith(".html"):
        fa = open("content/%s" % f, "r").read()

        # Get infos out of the variables (to define the article and keys)
        title = gett(fa, "Title: ", "\n")
        category = gett(fa, "Category: ", "\n")
        author = gett(fa, "Author: ", "\n")
        date = gett(fa, "Date: ", "\n")
        body = fa.split("Body:")[1]

        # Define the article and add it into the list
        article = Article(title, body, category, author, date)
        articles.append(article)

        # Add it into the keys
        try:
            categories[category].append(article)
        except:
            categories[category] = []
            categories[category].append(article)
        try:
            authors[author].append(article)
        except:
            authors[author] = []
            authors[author].append(article)
    elif f.endswith(".png") or f.endswith(".jpg"):
        dither("content/%s" % f, "output/%s" % f)
    else:
        copyfile("content/%s" % f, "output/%s" % f)

# Create the templates
inject(articles, "index.html", "output/index.html")

# Add the lists and keys
for article in articles:
    inject(article, "article.html", "output/%s.html" % article.title)
for category in categories:
    data = [category, categories[category]]
    inject(data, "category.html", "output/%s.html" % category)
for author in authors:
    data = [author, authors[author]]
    inject(data, "author.html", "output/%s.html" % author)

# Print success
print("Your generation is successful")
